# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 1987, 1993 Gordon V. Cormack, 2007 Nania Francesco Antonio,
#               Ilia Muraviev, Matt Mahoney, 2007 - 2021 Michael Ortmann

# tdm-gcc GCC compiler, Windows-friendly. https://jmeubank.github.io/tdm-gcc
gcc -march=native -O3 -fomit-frame-pointer -fwhole-program -s -Wall -fmodulo-sched freehook.c -o freehook
