/* SPDX-License-Identifier: GPL-2.0-only */
/* Copyright (c) 1987, 1993 Gordon V. Cormack, 2007 Nania Francesco Antonio,
 *               Ilia Muraviev, Matt Mahoney, 2007 - 2021 Michael Ortmann */

/*****************************************************************************
freehook v0.5 - ADMC file compressor
Based on Hook 0.8e

Hook was developed by Nania Francesco Antonio under GPL license.
For Version 0.9 he dropped that license and the tool is now closed source.
So i forked freehook from the last open source version which is 0.8e.
******************************************************************************
Hook v 0.8e ADMC file compressor. (C) 2007, Nania Francesco Antonio
(Italia - Meri (MESSINA))
under GPL, http://www.gnu.org/licenses/gpl.txt
Have invented: ADMC (Advanced DMC) (C) 2007 Nania Francesco Antonio.
Include LZP Preprocessor.
******************************************************************************
Special Thanks: (they have contributed)
Matt Mahoney,
Ilia Muraviev (EXE coder)
******************************************************************************
Considered all the modifications brought to the program in the evolution of
the same one has carried to me to create a being program to if that it has
implemented the DMC to maximum possible levels (the DMC it leave from two
fixed parameters limit=2 length=2) while the ADMC uses of the parameters
automati to us that they are created from an only parameter (limit=x). I have
removed completely the flush() in how much have left from the presupposed one
that the created structure with the memory to disposition was purer than that
one that would have been created with flush() (an operation that destroys the
data in possession). Therefore task of finally being able to coin the name of
this new more effective algorithm of the DMC from which it is born and to
which it is inspired.
This is the ADMC.
******************************************************************************
This program implements DMC, Copyright 1993, 1987
by Gordon V. Cormack, University of Waterloo, cormack@uwaterloo.ca
All rights reserved.
This code and the algorithms herein are the property of Gordon V. Cormack and
Frank Schwellinger. Neither the code nor any algorithm herein may be included
in any software, device, or process which is sold, exchanged for profit, or
for which a licence or royalty fee is charged. Permission is granted to use
this code for educational, research, or commercial purposes, provided this
notice is included, and provided this code is not used as described in the
above paragraph.
******************************************************************************
Description:
"a state machine in which each state represents a bitwise context. Each state
has 2 outgoing transitions corresponding to next bits 0 and 1, and a count n0
or n1 associated with each transition. Bit y (0 or 1) is compressed by
arithmetic coding with probability ny/(n0+n1)(where ny is n0 or n1 according
to y), and then ny is incremented. States are cloned (copied) whenever the
incoming and outgoing counts exceed certain limits. This has the effect of
creating a new context extended by 1 bit. In the example  below, the state
representing context 110 is cloned by creating a new state 0110 because the
incoming 0 transition count (ny for y=0) from state 11 exceeded a limit. This
transition is moved to point to the new state. Other incoming transitions (not
shown) remain pointing to the original state. The outgoing transitions are
copied. The counts of the original state are distributed to the new state in
proportion to the movedtransition's contribution to those counts, which is
w = ny/(n0+n1).
(Comment by Matt Mahoney: "The Paq data Compression Programs")
-> memory requirement:
ADMC->LZP + LZP buffer + File Buffer + ADMC = (32+64+32) 128 MB + x MB <-
*****************************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define FCT 1.0f

/******** VARIABLES DECLARATION *********************************************/

FILE *in, *out;
unsigned long long filesize;
unsigned long gsis1, gsis2, gsis3, ri1, ri2, ri3, gl1, gl2, gl3, rcp,
    addbra, addbra2, detec;
unsigned long maxsize, scanned, umax, rc1, rposo, ubi, rumax, ok, dimu,
    ramax, rcx, typec;
unsigned long el1, el2, di1, di2, el3, di3, esis1, esis2, exeat = 0;
unsigned long x2, x1, xmid, x, bit, pos, control, tot, *buf, repos, incre;
unsigned long digi1, digi2, digi3, digi4, mixu;
unsigned short memsize;
unsigned char mbit, memcre, nfi, nfo, profile, LZ, avan, limit;
char *nf, *extens;
float flimit1, flimit2;
typedef struct emubrain {
  struct emubrain *ahead[2];
  float cxt[2];
} branch;
branch *wizard, *sum, *branchmem, *maxbranch, *addrb, *limbra;
typedef struct emubrain2 {
  struct emubrain2 *ahead2[2];
  float cxt2[2];
} branch2;
branch2 *wizard2, *sum2, *branchmem2, *maxbranch2, *addrb2, *limbra2;
unsigned char *buffer;

/******** FUNCTIONS  ********************************************************/

// EXE AND DLL CODER BY ILIA MURAVIEV DERIVED BY QUAD COMPRESSOR MAX 16MB
static void e8e9transform(int t, int n)
{
  int i, addr;
  for (i = 0; i < (n - 4);) {
    switch (buffer[i++]) {
    case 0xe8:                 //-- CALL
    case 0xe9:                 //-- JMP
      addr = *((int *) (&buffer[i]));
      if (t) {
        if ((addr >= -i) && (addr < (n - i)))
          addr += i;
        else if ((addr > 0) && (addr < n))
          addr -= n;
      } else {
        if (addr < 0) {
          if ((addr + i) >= 0)
            addr += n;
        } else if (addr < n)
          addr -= i;
      }
      *((int *) (&buffer[i])) = addr;
      i += 4;
    }
  }
}

// PREDICTOR STRUCTURE
static void flush()
{
  int j, i;
  addrb = branchmem;
  for (j = 0; j < 256; j++)
    for (i = 0; i < 255; i++) {
      if (i < 127) {
        (addrb + j + (i << 8))->ahead[0] =
            addrb + j + (((i << 1) + 1) << 8);
        (addrb + j + (i << 8))->ahead[1] =
            addrb + j + (((i << 1) + 2) << 8);
      } else {
        (addrb + j + (i << 8))->ahead[0] = addrb + i + 1;
        (addrb + j + (i << 8))->ahead[1] = addrb + i - 127;
      }
      (addrb + j + (i << 8))->cxt[0] = FCT;
      (addrb + j + (i << 8))->cxt[1] = FCT;
    }
  addrb = branchmem + 65279;
  wizard = branchmem;
}

static void createbrain(unsigned long msize)
{
  // creation of Principal ADMC structure
  branchmem = (branch *) malloc(msize);
  if (branchmem == (branch *) NULL) {
    fprintf(stderr, "No Memory!\n");
    exit(1);
  }
  maxbranch = branchmem + (msize / sizeof(branch)) - 20;
  limbra = branchmem + (((msize / sizeof(branch)) / 32) * 24);
  addbra = ((msize / sizeof(branch)) - 21) / 32;
  flush();
}

static void flush2()
{
  int j, i;
  addrb2 = branchmem2;
  for (j = 0; j < 1024; j++)
    for (i = 0; i < 1024; i++) {
      if (i < 511) {
        (addrb2 + j + (i << 10))->ahead2[0] =
            addrb2 + j + (((i << 1) + 1) << 10);
        (addrb2 + j + (i << 10))->ahead2[1] =
            addrb2 + j + (((i << 1) + 2) << 10);
      } else {
        (addrb2 + j + (i << 10))->ahead2[0] = addrb2 + i + 1;
        (addrb2 + j + (i << 10))->ahead2[1] = addrb2 + i - 511;
      }
      (addrb2 + j + (i << 10))->cxt2[0] = FCT;
      (addrb2 + j + (i << 10))->cxt2[1] = FCT;
    }
  addrb2 = branchmem2 + (1 << 20);
  wizard2 = branchmem2;
}

static void createbrain2(unsigned long msize)
{
  // creation of LZP ADMC structure
  branchmem2 = (branch2 *) malloc(msize);
  if (branchmem2 == (branch2 *) NULL) {
    fprintf(stderr, "No Memory!\n");
    exit(1);
  }
  maxbranch2 = branchmem2 + (msize / sizeof(branch2)) - 20;
  limbra2 = branchmem2 + (((msize / sizeof(branch2)) / 32) * 24);
  addbra2 = ((msize / sizeof(branch2)) - 21) / 32;
  flush2();
}

// PROBABILITY LZP MODE
static float bp()
{
  return (float) ((float) wizard2->cxt2[1] /
                  ((float) wizard2->cxt2[1] + (float) wizard2->cxt2[0]));
}

// PROBABILITY  NORMAL MODE
static float p()
{
  return (float) ((float) wizard->cxt[1] /
                  ((float) wizard->cxt[1] + (float) wizard->cxt[0]));
}

// UPDATE NORMAL MODE
static void update(int y)
{
  float factor =
      wizard->cxt[y] / (wizard->ahead[y]->cxt[0] +
                        wizard->ahead[y]->cxt[1]);
  if (addrb < maxbranch
      && wizard->ahead[y]->cxt[0] + wizard->ahead[y]->cxt[1] >=
      (wizard->cxt[y] * factor) * (wizard->cxt[0] + wizard->cxt[1])
      && wizard->cxt[y] >= flimit1 + factor * factor) {
    addrb++;
    sum = addrb;
    sum->cxt[0] = wizard->ahead[y]->cxt[0] * factor;
    sum->cxt[1] = wizard->ahead[y]->cxt[1] * factor;
    sum->ahead[0] = wizard->ahead[y]->ahead[0];
    sum->ahead[1] = wizard->ahead[y]->ahead[1];
    wizard->ahead[y] = sum;
  }
  if (addrb == limbra)
    if (limbra + addbra <= maxbranch) {
      flimit1 += 0.5;
      limbra += addbra;
    }
  wizard->cxt[y]++;
  wizard = wizard->ahead[y];
}

// UPDATE ADMC AND PRESERVE MEMORY
static void pupdate(int y)
{
  wizard = wizard->ahead[y];
}

// UPDATE LZP MODE
static void bupdate(int y)
{
  float factor =
      wizard2->cxt2[y] / (wizard2->ahead2[y]->cxt2[0] +
                          wizard2->ahead2[y]->cxt2[1]);
  if (addrb2 < maxbranch2
      && wizard2->ahead2[y]->cxt2[0] + wizard2->ahead2[y]->cxt2[1] >=
      (wizard2->cxt2[y] * factor) * (wizard2->cxt2[0] + wizard2->cxt2[1])
      && wizard2->cxt2[y] >= flimit2 + factor * factor) {
    addrb2++;
    sum2 = addrb2;
    sum2->cxt2[0] = wizard2->ahead2[y]->cxt2[0] * factor;
    sum2->cxt2[1] = wizard2->ahead2[y]->cxt2[1] * factor;
    sum2->ahead2[0] = wizard2->ahead2[y]->ahead2[0];
    sum2->ahead2[1] = wizard2->ahead2[y]->ahead2[1];
    wizard2->ahead2[y] = sum2;
  }
  if (addrb2 == limbra2)
    if (limbra2 + addbra2 <= maxbranch2) {
      flimit2 += 0.5;
      limbra2 += addbra2;
    }
  wizard2->cxt2[y]++;
  wizard2 = wizard2->ahead2[y];
}

/*****************************************************************************
ENCODER/DECODER
This encoder/decoder is an evolution of the invented arithmetical coder from
Matt Mahoney (C) 2002, Nania Francesco Antonio (C) 2007.
*****************************************************************************/
static int decode()
{
  int y;
  if (ubi == 0)
    xmid = x1 + (unsigned long) ((x2 - x1) * p());      // FLOATING PRECISION
  else
    xmid = x1 + (unsigned long) ((x2 - x1) * bp());     // FLOATING PRECISION
  if (xmid < x1)
    xmid = x1;
  if (xmid >= x2)
    xmid = x2 - 1;
  if (x <= xmid) {
    y = 1;
    x2 = xmid;
  } else {
    y = 0;
    x1 = xmid + 1;
  }
  if (ubi == 0)
    update(y);
  else
    bupdate(y);
  // Shift equal MSB's out
  while (((x1 ^ x2) & 0xff0000) == 0) {
    x1 <<= 8;
    x2 = (x2 << 8) | 255;
    x = (x << 8) + getc(in);
    tot++;
  }
  return y;
}

static void encode(int y)
{
  if (ubi == 0)
    xmid = x1 + (unsigned long) ((x2 - x1) * p());      // FLOATING PRECISION
  else
    xmid = x1 + (unsigned long) ((x2 - x1) * bp());     // FLOATING PRECISION
  if (xmid < x1)
    xmid = x1;
  if (xmid >= x2)
    xmid = x2 - 1;
  if (y)
    x2 = xmid;
  else
    x1 = xmid + 1;
  if (ubi == 0)
    update(y);
  else
    bupdate(y);
  // Shift equal MSB's out
  while (((x1 ^ x2) & 0xff0000) == 0) {
    putc(x2 >> 16, out);
    tot++;
    x1 <<= 8;
    x2 = (x2 << 8) | 255;
  }
}

static void endx()
{
  printf
      ("        +---------------------------------------------------------------------------+\n"
       "        | freehook v0.5 - ADMC file compressor                                      |\n"
       "        | GNU General Public License v2.0 only                                      |\n"
       "        | Copyright (c) 1987, 1993 Gordon V. Cormack, 2007 Nania Francesco Antonio, |\n"
       "        | Ilia Muraviev, Matt Mahoney, 2007 - 2021 Michael Ortmann                  |\n"
       "        | Based on Hook 0.8e                                                        |\n"
       "        +---------------------------------------------------------------------------+\n"
       " To compress:   freehook c [memsize][limit][enable lz]{[lz step]} in out\n"
       " To decompress: freehook d in\n"
       " memsize        -> write MBytes [2-65535] ADMC buffer\n"
       " Limit          -> write [1-255]\n"
       " LZ compression -> write [0-1]  (0=disabled, 1=enabled)\n"
       " LZ step        -> write [1-255] (if enable LZP mode!)\n"
       " Example   compress: freehook c 256 2 0 in out     (LZ disabled)\n"
       " Example   compress: freehook c 256 2 1 255 in out (LZ enabled)\n"
       " Example decompress: freehook d in\n");
  exit(1);
}

static int Slen(char *f)
{
  unsigned char result = 0;
  while (*(f + result) != 0)
    result++;
  return result;
}

static void writename(char *n)
{
  int dpoint = 0, lex = 0, i;
  extens = (char *) malloc(256);
  unsigned char result = Slen(n);
  unsigned char s;
  fwrite(&result, 1, 1, out);
  for (i = 0; i < result; i++) {
    s = *(n + i);
    if (dpoint == 1) {
      *(extens + lex) = s;
      lex++;
    }
    if (s == 0x2e)
      dpoint = 1;
    fwrite(&s, 1, 1, out);
  }
}

static void readname()
{
  unsigned char result;
  char s;
  int dpoint = 0, lex = 0, i;
  extens = (char *) malloc(256);
  fread(&result, 1, 1, in);
  nf = (char *) malloc(result);
  for (i = 0; i < result; i++) {
    fread(&s, 1, 1, in);
    *(nf + i) = s;
  }
  *(nf + result) = 0;
  out = fopen(nf, "wb");
  printf("name=%s", nf);
  printf(" \n");
  if (!out)
    exit(1);
  for (i = 0; i < Slen(nf); i++) {
    s = *(nf + i);
    if (dpoint == 1) {
      *(extens + lex) = s;
      lex++;
    }
    if (s == 0x2e)
      dpoint = 1;
  }
  *(extens + lex) = 0;
}

static void detectf()
{
// PREFILTER SELECTION
  if (typec == 1) {
    profile = 0;
    exeat = 0;
    if (*buffer == 66 && *(buffer + 1) == 77)
      profile = 2;              // bmp
    if (*(buffer + 1) == 'I' && *(buffer + 2) == 'F'
        && *(buffer + 3) == 'F')
      profile = 2;              // ttf (*IFF)
    if (*buffer == 77 && *(buffer + 1) == 90
        && *(buffer + 2) == 144 && *(buffer + 3) == 0
        && *(buffer + 4) == 3 && *(buffer + 5) == 0
        && *(buffer + 6) == 0 && *(buffer + 7) == 0)
      profile = 4;              // exe
    if (*buffer == 37 && *(buffer + 1) == 80
        && *(buffer + 2) == 68 && *(buffer + 3) == 70)
      profile = 7;              // pdf
    if (*buffer == 63 && *(buffer + 1) == 95
        && *(buffer + 2) == 3 && *(buffer + 3) == 0)
      profile = 5;              // hlp
    if (*buffer == 208 && *(buffer + 1) == 207
        && *(buffer + 2) == 17 && *(buffer + 3) == 224)
      profile = 6;              // doc
    if (*buffer == 255 && *(buffer + 1) == 216
        && *(buffer + 2) == 255 && *(buffer + 3) == 224
        && *(buffer + 6) == 74 && *(buffer + 7) == 70
        && *(buffer + 8) == 73 && *(buffer + 9) == 70)
      profile = 8;              // jpg
    if (*buffer == 82 && *(buffer + 1) == 73
        && *(buffer + 2) == 70 && *(buffer + 3) == 70)
      profile = 11;             // wav
    if (*(buffer) == 6 && *(buffer + 4) == 68
        && *(buffer + 5) == 79 && *(buffer + 6) == 79
        && *(buffer + 7) == 77 && *(buffer + 8) == 32)
      profile = 12;             // save doom3
    if (*(buffer + 32768) == 01 && *(buffer + 32769) == 'C'
        && *(buffer + 32770) == 'D' && *(buffer + 32771) == '0'
        && *(buffer + 32772) == '0' && *(buffer + 32773) == '1'
        && *(buffer + 32774) == 01 && *(buffer + 32775) == 00)
      profile = 10;             // iso
    if (!strcmp(extens, "log") || !strcmp(extens, "LOG"))
      profile = 1;
    if (!strcmp(extens, "bmp") || !strcmp(extens, "BMP"))
      profile = 2;
    if (!strcmp(extens, "PNG") || !strcmp(extens, "PNG"))
      profile = 2;
    if (!strcmp(extens, "dic") || !strcmp(extens, "DIC"))
      profile = 3;
    if (!strcmp(extens, "exe") || !strcmp(extens, "EXE"))
      profile = 4;
    if (!strcmp(extens, "hlp") || !strcmp(extens, "HLP"))
      profile = 5;
    if (!strcmp(extens, "doc") || !strcmp(extens, "DOC"))
      profile = 6;
    if (!strcmp(extens, "xls") || !strcmp(extens, "XLS"))
      profile = 6;
    if (!strcmp(extens, "pdf") || !strcmp(extens, "PDF"))
      profile = 7;
    if (!strcmp(extens, "jpg") || !strcmp(extens, "JPG"))
      profile = 8;
    if (!strcmp(extens, "dll") || !strcmp(extens, "DLL"))
      profile = 4;
    if (!strcmp(extens, "txt") || !strcmp(extens, "TXT"))
      profile = 9;
    if (!strcmp(extens, "iso") || !strcmp(extens, "ISO"))
      profile = 10;
    if (!strcmp(extens, "wav") || !strcmp(extens, "WAV"))
      profile = 11;
    if (!strcmp(extens, "save") || !strcmp(extens, "SAVE"))
      profile = 12;
  }
  if (profile == 0) {
    ri1 = 1;
    ri2 = 1;
    ri3 = 4;
    gl2 = 7;
    gl3 = 14;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 1;
    di2 = 1;
    di3 = 2;
    el2 = 7;
    el3 = 14;
    esis1 = 1;
    esis2 = 1;
    mixu = 2;
    printf("Default\n");
  }
  if (profile == 1) {
    ri1 = 2;
    ri2 = 2;
    ri3 = 5;
    gl2 = 7;
    gl3 = 14;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 1;
    di2 = 1;
    di3 = 2;
    el2 = 7;
    el3 = 14;
    esis1 = 1;
    esis2 = 1;
    mixu = 4;
    printf("LOG file\n");
  }
  if (profile == 2) {
    ri1 = 0;
    ri2 = 0;
    ri3 = 0;
    gl2 = 8;
    gl3 = 16;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 4;
    di2 = 4;
    di3 = 4;
    el2 = 4;
    el3 = 8;
    esis1 = 1;
    esis2 = 1;
    mixu = 3;
    printf("IMAGE BMP file\n");
  }
  if (profile == 3) {
    ri1 = 6;
    ri2 = 6;
    ri3 = 6;
    gl2 = 2;
    gl3 = 4;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 0;
    di1 = 4;
    di2 = 4;
    el2 = 4;
    di3 = 4;
    el3 = 8;
    esis1 = 1;
    esis2 = 1;
    mixu = 2;
    printf("DIC file\n");
  }
  if (profile == 4) {
    ri1 = 0;
    ri2 = 0;
    ri3 = 0;
    gl2 = 8;
    gl3 = 16;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 4;
    di2 = 4;
    el2 = 4;
    di3 = 4;
    el3 = 8;
    esis1 = 1;
    esis2 = 1;
    mixu = 2;
    exeat = 1;
    printf("EXE file\n");
  }
  if (profile == 5) {
    ri1 = 0;
    ri2 = 0;
    ri3 = 0;
    gl2 = 8;
    gl3 = 16;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 0;
    di2 = 0;
    el2 = 8;
    di3 = 4;
    el3 = 16;
    esis1 = 1;
    esis2 = 1;
    mixu = 4;
    printf("HLP file\n");
  }
  if (profile == 6) {
    ri1 = 0;
    ri2 = 0;
    ri3 = 0;
    gl2 = 8;
    gl3 = 16;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 2;
    di2 = 2;
    el2 = 6;
    di3 = 2;
    el3 = 12;
    esis1 = 1;
    esis2 = 1;
    mixu = 4;
    printf("DOC file\n");
  }
  if (profile == 7) {
    ri1 = 0;
    ri2 = 0;
    ri3 = 0;
    gl2 = 8;
    gl3 = 16;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 4;
    di2 = 4;
    el2 = 4;
    di3 = 4;
    el3 = 8;
    esis1 = 1;
    esis2 = 1;
    mixu = 4;
    printf("PDF file\n");
  }
  if (profile == 8) {
    ri1 = 0;
    ri2 = 0;
    ri3 = 0;
    gl2 = 8;
    gl3 = 16;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 4;
    di2 = 4;
    el2 = 4;
    di3 = 4;
    el3 = 8;
    esis1 = 1;
    esis2 = 1;
    mixu = 1;
    printf("IMAGE JPEG file\n");
  }
  if (profile == 9) {
    ri1 = 4;
    ri2 = 4;
    ri3 = 4;
    gl2 = 4;
    gl3 = 8;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 0;
    di1 = 0;
    di2 = 0;
    el2 = 8;
    di3 = 4;
    el3 = 16;
    esis1 = 1;
    esis2 = 1;
    mixu = 2;
    printf("TXT file\n");
  }
  if (profile == 10) {
    ri1 = 1;
    ri2 = 1;
    ri3 = 4;
    gl2 = 7;
    gl3 = 14;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 0;
    di2 = 0;
    el2 = 8;
    di3 = 4;
    el3 = 16;
    esis1 = 1;
    esis2 = 1;
    mixu = 2;
    printf("ISO image \n");
  }
  if (profile == 11) {
    ri1 = 6;
    ri2 = 6;
    ri3 = 2;
    gl2 = 6;
    gl3 = 4;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 2;
    di2 = 2;
    el2 = 6;
    di3 = 2;
    el3 = 12;
    esis1 = 1;
    esis2 = 1;
    mixu = 4;
    printf("WAV file\n");
  }
  if (profile == 12) {
    ri1 = 0;
    ri2 = 0;
    ri3 = 0;
    gl2 = 8;
    gl3 = 16;
    gsis1 = 1;
    gsis2 = 1;
    gsis3 = 1;
    di1 = 1;
    di2 = 1;
    el2 = 7;
    di3 = 2;
    el3 = 14;
    esis1 = 1;
    esis2 = 1;
    mixu = 3;
    printf("SAVE file\n");
  }
}

static void cwrite(unsigned char c)
{
  int i;
  ubi = 0;
  for (i = 7; i >= 0; --i)
    encode((c >> i) & 1);
}

static void cread()
{
  unsigned char c;
  int i;
  ubi = 0;
  c = 0;
  for (i = 7; i >= 0; --i)
    c += c + decode();
  *(buffer + pos) = c;
}

/******** MAIN **************************************************************/

int main(int argc, char **argv)
{
  int f, i, ni, l;
  // Check arguments: hook c/d option in out
  if (argc > 6 && argc < 9 && argv[1][0] == 'c' && isdigit(*argv[2]))
    sscanf(argv[2], "%lu", &digi1);
  if (argc > 6 && argc < 9 && argv[1][0] == 'c' && isdigit(*argv[3]))
    sscanf(argv[3], "%lu", &digi2);
  if (argc > 6 && argc < 9 && argv[1][0] == 'c' && isdigit(*argv[4]))
    sscanf(argv[4], "%lu", &digi3);
  if (argc == 8 && argv[1][0] == 'c' && digi3 == 1 && isdigit(*argv[5]))
    sscanf(argv[5], "%lu", &digi4);
  memsize = (unsigned short) digi1;
  limit = (unsigned char) digi2;
  LZ = (unsigned char) digi3;
  avan = (unsigned char) digi4;
  if (argc < 3 || argc > 8) {
    endx();
    return 1;
  }
  if (argc == 7 && LZ != 0 && argv[1][0] == 'c') {
    endx();
    return 1;
  }
  if (argc == 8 && LZ != 1 && argv[1][0] == 'c') {
    endx();
    return 1;
  }
  if (argc != 7 + LZ && argv[1][0] == 'c') {
    endx();
    return 1;
  }
  if (argv[1][0] != 'c' && argv[1][0] != 'd') {
    endx();
    return 1;
  }
  if (argv[1][0] == 'c' && LZ == 1
      && (digi1 < 2 || digi1 > 65535 || digi2 < 1 || digi4 < 1
          || digi2 > 255 || digi4 > 255)) {
    endx();
  }
  if (argv[1][0] == 'c' && LZ == 0
      && (digi1 < 2 || digi1 > 65535 || digi2 < 1 || digi2 > 255)) {
    endx();
  }
  if (argv[1][0] == 'd' && argc != 3) {
    endx();
  }
  clock_t start = clock();      // Start timer
  control = start;
  printf
      ("        +---------------------------------------------------------------------------+\n"
       "        | freehook v0.5 - ADMC file compressor                                      |\n"
       "        | GNU General Public License v2.0 only                                      |\n"
       "        | Copyright (c) 1987, 1993 Gordon V. Cormack, 2007 Nania Francesco Antonio, |\n"
       "        | Ilia Muraviev, Matt Mahoney, 2007 - 2021 Michael Ortmann                  |\n"
       "        | Based on Hook 0.8e                                                        |\n"
       "        +---------------------------------------------------------------------------+\n");
  // Open file
  if (argv[1][0] == 'c') {
    typec = 1;
    printf("Memory=%d MBytes Limit=%d ", memsize, limit);
    if (LZ)
      printf("LZ enabled step %d\n", avan);
    else
      printf("LZ disabled\n");
    in = fopen(argv[5 + LZ], "rb");
    if (!in)
      perror(argv[5 + LZ]), exit(1);
    out = fopen(argv[6 + LZ], "wb");
    if (!out)
      perror(argv[6 + LZ]), exit(1);
    nfi = 5 + LZ;
    nfo = 6 + LZ;
  } else {
    typec = 0;
    in = fopen(argv[2], "rb");
    if (!in)
      perror(argv[2]), exit(1);
    nfi = 2;
    nfo = 3;
  }
  int c;
  x1 = 0;                       // min
  x2 = 0xffffff;                // max
  x = 0;
  maxsize = 1 << 26;            // data read buffer
  buffer = (unsigned char *) malloc(maxsize + 3);
  buffer[0] = 0;
  buffer[1] = 0;
  buffer[2] = 0;
  buffer += 3;
  buf = (unsigned long *) malloc(sizeof(unsigned long) << 24);
  // Compress file
  if (argv[1][0] == 'c') {
    flimit1 = (float) limit;
    flimit2 = (float) limit;
    fseek(in, 0, SEEK_END);
    filesize = ftell(in);
    control = filesize / 100;
    rewind(in);
    fwrite(&filesize, 1, 8, out);
    fwrite(&memsize, 1, 2, out);
    fwrite(&limit, 1, 1, out);
    fwrite(&LZ, 1, 1, out);
    if (LZ == 1)
      fwrite(&avan, 1, 1, out);
    writename(argv[5 + LZ]);
    createbrain(memsize * 1000000);
    while (scanned < filesize) {
      if (scanned + maxsize > filesize)
        maxsize = filesize - scanned;
      pos = 0;
      umax = 0;
      fread(buffer, 1, maxsize, in);    // read maxsixe data
      if (detec == 0) {
        detec = 1;
        detectf();
        fwrite(&profile, 1, 1, out);
      }                         // detect profile
      if (exeat == 1)
        e8e9transform(1, maxsize);      // EXE E8E9 Coder by Ilia Muraviev activated
      if (LZ == 1)              // LZP MODE activated
      {
        if (memcre == 0) {
          createbrain2(1 << 25);
          memcre = 1;
        }                       // opend ADMC for LZP
        memset(buf, 0, sizeof(unsigned long) << 24);
        while (pos < maxsize) {
          c = *(buffer + pos);
          rc1 =
              (gsis1 * (*(buffer + pos - 1) >> ri1)) +
              (gsis2 * (*(buffer + pos - 2) >> ri2) << gl2) +
              (gsis3 * (*(buffer + pos - 3) >> ri3) << gl3);
          if (buf[rc1] < mixu + 3
              || memcmp(buffer + (buf[rc1] - 1) - mixu,
                        buffer + pos - mixu, mixu) != 0)
            cwrite(c);
          else {
            rcx =
                (*(buffer + pos - 1) >> di1) +
                (esis1 * (*(buffer + pos - 2) >> di2) << el2) +
                (esis2 * (*(buffer + pos - 3) >> di3) << el3);
            rposo = buf[rc1] - 1;
            while (memcmp(buffer + rposo + umax, buffer + pos + umax, avan)
                   == 0 && pos + umax + avan <= maxsize) {
              umax += avan;
              ramax++;
            }
            wizard2 = branchmem2 + rcx; // FOR BEST DMC ENCODING REDIRECTION
            if (umax == 0)      // no LZ
            {
              umax = 0;
              ubi = 1;
              encode(0);
              cwrite(c);
            } else {
              for (f = 0; f < umax; f++)
                for (i = 7; i >= 0; --i)
                  pupdate((*(buffer + rposo + f) >> i) & 1);
              ubi = 1;
              umax--;
              rumax = ramax;
              for (i = 1; i < 32; i++) {
                encode(1);
                if (rumax <= (1 << i)) {
                  encode(0);
                  for (ni = i - 1; ni >= 0; --ni)
                    encode(((rumax - 1) >> ni) & 1);
                  break;
                }
                rumax -= (1 << i);
              }
            }
          }
          if (scanned + pos > control) {
            printf("pack %s %lu to %lu bytes, ratio %f\r", argv[6],
                   (scanned + pos), tot, (float) tot / (scanned + pos));
            control += filesize / 100;
          }
          buf[rc1] = ++pos;
          pos += umax;
          umax = 0;
          ramax = 0;
        }
      } else                    // NORMAL ADMC MODE
      {
        while (pos < maxsize) {
          c = (unsigned char) *(buffer + pos);
          cwrite(c);
          if (scanned + pos > control) {
            printf("pack %s %lu bytes to %s %lu bytes, ratio %f\r",
                   argv[5], (scanned + pos), argv[6], tot,
                   (float) tot / (scanned + pos));
            control += filesize / 100;
          }
          pos++;
        }
      }

      scanned += maxsize;
    }

    for (i = 2; i >= 0; --i)    // Flush x2
      putc(x2 >> (i << 3), out);
  }
  // Decompression of file
  else {
    fread(&filesize, 1, 8, in);
    fread(&memsize, 1, 2, in);
    fread(&limit, 1, 1, in);
    fread(&LZ, 1, 1, in);
    flimit1 = (float) limit;
    flimit2 = (float) limit;
    if (LZ == 1)
      fread(&avan, 1, 1, in);
    readname();
    fread(&profile, 1, 1, in);
    detectf();
    createbrain(memsize * 1000000);
    for (i = 2; i >= 0; --i)    // get x
      x += getc(in) << (i << 3);
    while (scanned < filesize) {
      if (scanned + maxsize > filesize)
        maxsize = filesize - scanned;
      pos = 0;
      umax = 0;
      // LZP MODE ACTIVATED
      if (LZ == 1) {
        if (memcre == 0) {
          createbrain2(1 << 25);
          memcre = 1;
        }
        memset(buf, 0, sizeof(unsigned long) << 24);
        while (pos < maxsize) {
          rc1 =
              (gsis1 * (*(buffer + pos - 1) >> ri1)) +
              (gsis2 * (*(buffer + pos - 2) >> ri2) << gl2) +
              (gsis3 * (*(buffer + pos - 3) >> ri3) << gl3);
          if (buf[rc1] < mixu + 3
              || memcmp(buffer + (buf[rc1] - 1) - mixu,
                        buffer + pos - mixu, mixu) != 0)
            cread();
          else {
            rcx =
                (*(buffer + pos - 1) >> di1) +
                (esis1 * (*(buffer + pos - 2) >> di2) << el2) +
                (esis2 * (*(buffer + pos - 3) >> di3) << el3);
            wizard2 = branchmem2 + rcx; // FOR BEST DMC ENCODING REDIRECTION
            ubi = 1;
            ramax = 0;
            incre = 0;
            while (decode() > 0) {
              ramax += (1 << incre);
              incre++;
            }
            if (incre > 0) {
              c = 0;
              for (i = incre - 1; i >= 0; --i)
                c += c + decode();
              ramax += c;
            }
            if (ramax == 0) {
              cread();
            }                   // NO LZP
            else {
              umax = ramax * avan - 1;
              for (l = 0; l <= umax; l++) {
                *(buffer + pos + l) = *(buffer + (buf[rc1] - 1) + l);
                for (i = 7; i >= 0; --i)
                  pupdate((*(buffer + (buf[rc1] - 1) + l) >> i) & 1);
              }
            }
          }
          if (scanned + pos > control) {
            printf("unpack %s to %s %lu / %llu bytes\r", argv[2], nf,
                   scanned + pos, filesize);
            control += filesize / 100;
          }
          buf[rc1] = pos + 1;
          pos += 1 + umax;
          umax = 0;
        }
      } else
        while (pos < maxsize)   // NORMAL MODE
        {
          cread();
          if (scanned + pos > control) {
            printf("unpack %s to %s %lu / %llu bytes\r", argv[2], nf,
                   scanned + pos, filesize);
            control += filesize / 100;
          }
          pos++;
        }
      if (exeat == 1)
        e8e9transform(0, maxsize);
      fwrite(buffer, 1, maxsize, out);
      scanned += maxsize;
    }
  }
  // Print results
  if (argv[1][0] == 'c')
    nf = argv[nfo];
  printf("%s (%ld) to %s (%ld) in %1.2fs. %iKB/s\n",
         argv[nfi], ftell(in), nf, ftell(out),
         ((double) clock() - start) / CLOCKS_PER_SEC,
         (int) (filesize / ((double) clock() - start)));
  return 0;
}
